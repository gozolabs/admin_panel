// document ready function
$(document).ready(function() { 	


	//--------------------- Pagination Settings ---------------------//

    /* initiate pending pagination */
    $("#pendingHolder").jPages({
      containerID : "pendingContainer",
      perPage : 8
    });

    /* on select change */
    $("select").change(function(){
      /* get new nº of items per page */
      var newPerPage = parseInt( $(this).val() );

      /* destroy jPages and initiate plugin again */
      $("#pendingHolder").jPages("destroy").jPages({
        containerID   : "pendingContainer",
        perPage       : newPerPage
      });
    });    

    /* initiate stop pagination */
    $("#stopHolder").jPages({
      containerID : "stopContainer",
      perPage : 8
    });

    /* on select change */
    $("select").change(function(){
      /* get new nº of items per page */
      var newPerPage = parseInt( $(this).val() );

      /* destroy jPages and initiate plugin again */
      $("#stopHolder").jPages("destroy").jPages({
        containerID   : "stopContainer",
        perPage       : newPerPage
      });
    });    

	/* initiate start pagination */
    $("#activeHolder").jPages({
      containerID : "activeContainer",
      perPage : 8
    });

    /* on select change */
    $("select").change(function(){
      /* get new nº of items per page */
      var newPerPage = parseInt( $(this).val() );

      /* destroy jPages and initiate plugin again */
      $("#activeHolder").jPages("destroy").jPages({
        containerID   : "activeContainer",
        perPage       : newPerPage
      });
    });


	//------------- Check all checkboxes  -------------//
	
	$("#masterActiveCh").click(function() {
		var checkedStatus = $(this).find('span').hasClass('checked');
		$("#checkActiveAll tr .chChildren input:checkbox").each(function() {
			this.checked = checkedStatus;
				if (checkedStatus == this.checked) {
					$(this).closest('.checker > span').removeClass('checked');
				}
				if (this.checked) {
					$(this).closest('.checker > span').addClass('checked');
				}
		});
	});

		
	$("#masterPendCh").click(function() {
		var checkedStatus = $(this).find('span').hasClass('checked');
		$("#checkPendAll tr .chChildren input:checkbox").each(function() {
			this.checked = checkedStatus;
				if (checkedStatus == this.checked) {
					$(this).closest('.checker > span').removeClass('checked');
				}
				if (this.checked) {
					$(this).closest('.checker > span').addClass('checked');
				}
		});
	});

		
	$("#masterStopCh").click(function() {
		var checkedStatus = $(this).find('span').hasClass('checked');
		$("#checkStopAll tr .chChildren input:checkbox").each(function() {
			this.checked = checkedStatus;
				if (checkedStatus == this.checked) {
					$(this).closest('.checker > span').removeClass('checked');
				}
				if (this.checked) {
					$(this).closest('.checker > span').addClass('checked');
				}
		});
	});


	//Boostrap modal
	$('#myModal').modal({ show: false});
	
	//add event to modal after closed
	$('#myModal').on('hidden', function () {
	  	console.log('modal is closed');
	})


});//End document ready functions

