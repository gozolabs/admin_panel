// document ready function
$(document).ready(function() { 	


    //--------------- Data tables ------------------//
  if($('table').hasClass('dynamicTable')){
    $('.dynamicTable').dataTable({
      "sPaginationType": "full_numbers",
      "bJQueryUI": false,
      "bAutoWidth": false,
      "bLengthChange": false,
      "fnInitComplete": function(oSettings, json) {
          $('.dataTables_filter>label>input').attr('id', 'search');
        }
    });
  }

  //------------- Check all checkboxes  -------------//
  
  $("#masterCh").click(function() {
    var checkedStatus = $(this).find('span').hasClass('checked');
    $("#checkAll tr .chChildren input:checkbox").each(function() {
      this.checked = checkedStatus;
        if (checkedStatus == this.checked) {
          $(this).closest('.checker > span').removeClass('checked');
        }
        if (this.checked) {
          $(this).closest('.checker > span').addClass('checked');
        }
    });
  });

  function isCheckboxChecked() {
    var isChecked = false;

    $("#checkAll tr .chChildren input:checkbox").each(function() {
      if ($(this).closest('.checker > span').hasClass('checked'))
        isChecked = true;
    });
    return isChecked;
  }

  //--------------------- Smart Wizard Settings ---------------------//

  $('#wizard').smartWizard({
    transitionEffect: 'fade', // Effect on navigation, none/fade/slide/
    onLeaveStep:leaveAStepCallback,
    onFinish:onFinishCallback
  });

  function leaveAStepCallback(obj){
      var step = obj;
      var stepN = step.attr('rel');

      //validate step 1
      if(stepN == 1) {

        var validatePdt = isCheckboxChecked();

        if(validatePdt == true) {
          step.find('.stepNumber').stop(true, true).remove();
          step.find('.stepDesc').stop(true, true).before('<span class="stepNumber"><span class="icon16 iconic-icon-checkmark"></span></span>');
          console.log("is true");
          return true;
        } else {
          console.log("is false");
          return false;
        }
      }

      //validate step 2
      if (stepN == 2) {
        if ($("#shareDsc").valid() == true ) {
            var validateShr = true;
        } else {
          var validateShr = false;
        }
        if ($("#bwRewards").valid() == true ) {
            var validateRw = true;
        } else {
          var validateRw = false;
        }        
        if ($("#bwMax").valid() == true ) {
            var validateMax = true;
        } else {
          var validateMax = false;
        }
        if(validateShr == true && validateRw == true && validateMax == true) {
          step.find('.stepNumber').stop(true, true).remove();
          step.find('.stepDesc').stop(true, true).before('<span class="stepNumber"><span class="icon16 iconic-icon-checkmark"></span></span>');
          return true;
        } else {
          return false;
        }
      }

      //validate step 3
      if (stepN == 3) {
        if ($("#startDatePicker").valid() == true ) {
            var validateStartDate = true;
        } else {
          var validateStartDate = false;
        }
        if ($("#endDatePicker").valid() == true ) {
            var validateEndDate = true;
        } else {
          var validateEndDate = false;
        }                
        if ($("#validityInput").valid() == true ) {
            var validateValidInput = true;
        } else {
          var validateValidInput = false;
        }        

        if(validateStartDate == true && validateEndDate == true && validateValidInput == true) {
          step.find('.stepNumber').stop(true, true).remove();
          step.find('.stepDesc').stop(true, true).before('<span class="stepNumber"><span class="icon16 iconic-icon-checkmark"></span></span>');
          return true;
        } else {
          return false;
        }
      }      

      //validate step 4
      if (stepN == 4) {
        step.find('.stepNumber').stop(true, true).remove();
        step.find('.stepDesc').stop(true, true).before('<span class="stepNumber"><span class="icon16 iconic-icon-checkmark"></span></span>');
        return true;
      }
  }

  function onFinishCallback(obj){
    var step = obj;
    step.find('.stepNumber').stop(true, true).remove();
      step.find('.stepDesc').stop(true, true).before('<span class="stepNumber"><span class="icon16 iconic-icon-checkmark"></span></span>');
      $.pnotify({
        type: 'success',
        title: 'Done',
        text: 'You finish the wizzard',
        icon: 'picon icon16 iconic-icon-check-alt white',
        opacity: 0.95,
        history: false,
        sticker: false
     });
  }


  //--------------- Form validation ------------------//
  $("#wizard-form").validate({

    rules: {
      prdtTable: {
        required: true,
      },
      shareDsc: {
        required: true,
        number: true
      },
      bwRewards: {
        required: true,
        number: true
      },
      bwMax: {
        required: true,
        number: true
      },
      startDatePicker: {
        required: true,
        date: true
      },      
      endDatePicker: {
        required: true,
        date: true
      },
      validityInput: {
        required: true,
        number: true
      }
    },
    messages: {
      prdtTable:  "This field is required",
      shareDsc: {
        required: "This field is required",
        number: "Input a number for Sharing Discount",
      },
      bwRewards: {
        required: "This field is required",
        number: "Input a number for Bandwagon Rewards",
      },
      bwMax: {
        required: "This field is required",
        number: "Input a number for Bandwagon Max Rewards",
      },      
      startDatePicker: {
        required: "This field is required",
        date: "Please input a valid date",
      },      
      endDatePicker: {
        required: "This field is required",
        date: "Please input a valid date",
      },
      validityInput: {
        required: "This field is required",
        number: "Input a number for coupon validity",
      }
    } 
  });

  //Profit Margin range
  $( "#gmSlider" ).slider({
    range: "min",
    value:0,
    min: 0,
    max: 100,
    step: 1,
    slide: function( event, ui ) {
      $( "#gmPercent" ).val(ui.value + "%");
    }
  });
  $( "#gmPercent" ).val( $( "#gmSlider" ).slider( "value" )  + "%");

  $( "#emSlider" ).slider({
    range: "min",
    value:0,
    min: 0,
    max: 100,
    step: 1,
    slide: function( event, ui ) {
      $( "#emPercent" ).val(ui.value + "%");
    }
  });
  $( "#emPercent" ).val( $( "#emSlider" ).slider( "value" )  + "%");


  //------------- Datepicker -------------//
  if($('#startDatePicker').length) {
    $("#startDatePicker").datepicker({
      showOtherMonths:true
    });
  }  

  if($('#endDatePicker').length) {
    $("#endDatePicker").datepicker({
      showOtherMonths:true
    });
  }


	//Boostrap modal
	$('#myModal').modal({ show: false});
	
	//add event to modal after closed
	$('#myModal').on('hidden', function () {
	  	console.log('modal is closed');
	})


});//End document ready functions

